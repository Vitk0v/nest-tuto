import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { Piano, Prisma } from '@prisma/client';
import { PianoInterface } from './piano.interface';

@Injectable()
export class PianoRepository {
  constructor(private prisma: PrismaService) {}

  async addPiano(params: { data: Prisma.PianoCreateInput }): Promise<Piano> {
    const { data } = params;
    return this.prisma.piano.create({ data });
  }

  async getPianoByID(id: number): Promise<Piano> {
    return this.prisma.piano.findUnique({ where: { id: id } });
  }

  async getPianos(params: PianoInterface): Promise<Piano[]> {
    const { sold, price } = params;
    return this.prisma.piano.findMany({
      where: {
        ...(sold ? { sold: sold } : {}),
        ...(price ? { price: price } : {}),
      },
    });
  }

  async updatePianoByID(
    id: number,
    updatePiano: PianoInterface,
  ): Promise<Piano> {
    return this.prisma.piano.update({
      where: { id: id },
      data: updatePiano,
    });
  }

  async deletePianoByID(id: number): Promise<Piano> {
    return this.prisma.piano.delete({
      where: { id: id },
    });
  }
}
