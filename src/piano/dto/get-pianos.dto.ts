import { IsBoolean, IsNumber, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetPianosDTO {
  @IsOptional()
  @IsNumber()
  @Transform(({ value }) => parseInt(value, 10))
  price?: number;

  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  sold?: boolean;
}
