import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { PianoEntity } from './entities/piano.entity';
import { GetPianosDTO } from './dto/get-pianos.dto';
import { PianoDTO } from './dto/piano.dto';
import { PianoService } from './piano.service';
import { Public } from '../auth/auth.guard';

@Controller('piano')
export class PianoController {
  constructor(private pianoService: PianoService) {}

  @Public()
  @Get()
  async getPianos(@Query() params: GetPianosDTO): Promise<PianoEntity[]> {
    return await this.pianoService.getPianos(params);
  }

  @Public()
  @Get('/:id')
  async getPianoByID(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<PianoEntity> {
    return await this.pianoService.getPianoByID(id);
  }

  @Post()
  async addPiano(@Body() newPiano: PianoDTO): Promise<PianoEntity> {
    return await this.pianoService.addPiano(newPiano);
  }

  @Put('/:id')
  async updatePianoByID(
    @Param('id', ParseIntPipe) id: number,
    @Body() updatePiano: Partial<PianoDTO>,
  ): Promise<PianoEntity> {
    return await this.pianoService.updatePianoByID(id, updatePiano);
  }

  @Delete('/:id')
  async deletePianoByID(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<{ message: string }> {
    return await this.pianoService.deletePianoByID(id);
  }
}
