import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { UserRepository } from './user.repository';
import { UserEntity } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { UserInterface } from './user.interface';

@Injectable()
export class UserService {
  constructor(private repository: UserRepository) {}

  async getUsers(): Promise<UserEntity[]> {
    return await this.repository.getUsers();
  }

  async getUserByID(id: number): Promise<UserEntity> {
    const user: UserEntity = await this.repository.getUserByID(id);
    if (user) {
      return user;
    } else {
      throw new NotFoundException(`Le user d'ID ${id} n'existe pas.`);
    }
  }

  async getUserByEmail(email: string): Promise<UserEntity> {
    const user: UserEntity = await this.repository.getUserByEmail(email);
    if (user) {
      return user;
    } else {
      throw new NotFoundException(`Aucun user n'a l'email: ${email}.`);
    }
  }

  async addUser(newUser: UserInterface): Promise<{ message: string }> {
    const { email, password } = newUser;
    const bcryptPassword = await bcrypt.hash(password, 12);
    try {
      await this.repository.addUser({
        data: {
          email,
          password: bcryptPassword,
        },
      });
      return { message: `Le user ${email} a été créé.` };
    } catch {
      throw new ConflictException(`L'email ${email} est déjà utilisé`);
    }
  }

  async updateUserByID(
    id: number,
    updateUser: UserInterface,
  ): Promise<UserEntity> {
    try {
      if (updateUser.password) {
        updateUser.password = await bcrypt.hash(updateUser.password, 12);
      }
      return await this.repository.updateUserByID(id, updateUser);
    } catch {
      throw new NotFoundException(`Le user d'ID ${id} n'existe pas.`);
    }
  }

  async deleteUserByID(id: number): Promise<{ message: string }> {
    try {
      await this.repository.deleteUserByID(id);
      return { message: `Le user d'ID ${id} a été supprimé.` };
    } catch {
      throw new NotFoundException(`Le user d'ID ${id} n'existe pas.`);
    }
  }
}
