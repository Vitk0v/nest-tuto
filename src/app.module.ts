import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { PianoModule } from './piano/piano.module';
import { BlueMiddleware } from './middlewares/blue/blue.middleware';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';

@Module({
  imports: [PianoModule, UserModule, AuthModule],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer
      .apply(BlueMiddleware)
      .forRoutes(
        { path: 'piano*', method: RequestMethod.POST },
        { path: 'piano*', method: RequestMethod.PUT },
        { path: 'piano*', method: RequestMethod.DELETE },
      );
  }
}
