import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';
import { User, Prisma } from '@prisma/client';
import { UserInterface } from './user.interface';

@Injectable()
export class UserRepository {
  constructor(private prisma: PrismaService) {}

  async addUser(params: { data: Prisma.UserCreateInput }): Promise<User> {
    const { data } = params;
    return this.prisma.user.create({ data });
  }

  async getUserByEmail(email: string): Promise<User> {
    return this.prisma.user.findUnique({ where: { email: email } });
  }

  async getUserByID(id: number): Promise<User> {
    return this.prisma.user.findUnique({ where: { id: id } });
  }

  async getUsers(): Promise<User[]> {
    return this.prisma.user.findMany();
  }

  async updateUserByID(id: number, updateUser: UserInterface): Promise<User> {
    return this.prisma.user.update({
      where: { id: id },
      data: updateUser,
    });
  }

  async deleteUserByID(id: number): Promise<User> {
    return this.prisma.user.delete({
      where: { id: id },
    });
  }
}
