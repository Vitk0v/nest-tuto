import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class BlueMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: () => void) {
    console.log(`Blue Middleware are active on ${req.method} method`);
    next();
  }
}
