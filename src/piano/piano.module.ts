import { Module } from '@nestjs/common';
import { PianoController } from './piano.controller';
import { PianoService } from './piano.service';
import { PrismaModule } from '../prisma/prisma.module';
import { PianoRepository } from './piano.repository';

@Module({
  imports: [PrismaModule],
  exports: [],
  controllers: [PianoController],
  providers: [PianoService, PianoRepository],
})
export class PianoModule {}
