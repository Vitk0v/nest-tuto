export class PianoEntity {
  id: number;
  name: string;
  price: number;
  sold: boolean;
  createdAt: Date;
  updatedAt: Date;
}
