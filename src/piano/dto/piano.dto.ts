import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class PianoDTO {
  @IsString()
  @IsNotEmpty()
  @MinLength(6, {
    message: 'La taille minimale du champ name est de 6 caractères',
  })
  @MaxLength(10, {
    message: 'La taille maximale du champ name est de 10 caractères',
  })
  name: string;

  @IsNumber()
  @IsNotEmpty()
  price: number;

  @IsBoolean()
  @IsNotEmpty()
  sold: boolean;
}
