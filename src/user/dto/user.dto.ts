import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class UserDTO {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6, {
    message: 'La taille minimale du champ password est de 6 caractères',
  })
  @MaxLength(12, {
    message: 'La taille maximale du champ password est de 12 caractères',
  })
  password: string;
}
