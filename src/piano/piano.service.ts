import { Injectable, NotFoundException } from '@nestjs/common';
import { PianoEntity } from './entities/piano.entity';
import { PianoRepository } from './piano.repository';
import { PianoInterface } from './piano.interface';

@Injectable()
export class PianoService {
  constructor(private repository: PianoRepository) {}

  async getPianos(params?: PianoInterface): Promise<PianoEntity[]> {
    return await this.repository.getPianos(params);
  }

  async getPianoByID(id: number): Promise<PianoEntity> {
    const piano: PianoEntity = await this.repository.getPianoByID(id);
    if (piano) {
      return piano;
    } else {
      throw new NotFoundException(`Le piano d'ID ${id} n'existe pas.`);
    }
  }

  async addPiano(newPiano: PianoInterface): Promise<PianoEntity> {
    const { name, price, sold } = newPiano;
    return await this.repository.addPiano({
      data: {
        name,
        price,
        sold,
      },
    });
  }

  async updatePianoByID(
    id: number,
    updatePiano: PianoInterface,
  ): Promise<PianoEntity> {
    try {
      return await this.repository.updatePianoByID(id, updatePiano);
    } catch {
      throw new NotFoundException(`Le piano d'ID ${id} n'existe pas.`);
    }
  }

  async deletePianoByID(id: number): Promise<{ message: string }> {
    try {
      await this.repository.deletePianoByID(id);
      return { message: `Le piano d'ID ${id} a été supprimé.` };
    } catch {
      throw new NotFoundException(`Le piano d'ID ${id} n'existe pas.`);
    }
  }
}
