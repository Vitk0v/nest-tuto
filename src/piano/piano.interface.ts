export type PianoInterface = {
  name?: string;
  price?: number;
  sold?: boolean;
};
