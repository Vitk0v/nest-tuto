# Base image
FROM node:20-alpine

# Create app directory
WORKDIR /usr/app

# Install PM2 globally
RUN npm install --global pm2

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install dependencies
RUN npm install --omit=dev

# Bundle app source
COPY . .

# Copy the .env and .env.development files
COPY .env ./

# Creates a "dist" folder with the production build
RUN npm run build

# Expose the port on which the app will run
EXPOSE 3000

# Start the server using the production build
CMD [ "npm", "run", "start:prod" ]