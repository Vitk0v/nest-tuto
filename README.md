## Config

1. COPY and rename `.env.example` into `.env`
2. Edit info (if necessary) of your new `.env`

## DB

```bash
npx prisma migrate dev --name init
```

## Installation

```bash
# use node v20.11.1
$ nvm use

# install node_modules
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
