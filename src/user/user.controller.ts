import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserEntity } from './entities/user.entity';
import { UserDTO } from './dto/user.dto';

@Controller('user')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  async getUsers(): Promise<UserEntity[]> {
    return await this.userService.getUsers();
  }

  @Get('/:id')
  async getUserByID(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<UserEntity> {
    return await this.userService.getUserByID(id);
  }

  @Get('/email/:email')
  async getUserByEmail(@Param('email') email: string): Promise<UserEntity> {
    return await this.userService.getUserByEmail(email);
  }

  @Post()
  async addUser(@Body() newUser: UserDTO): Promise<{ message: string }> {
    return await this.userService.addUser(newUser);
  }

  @Put('/:id')
  async updateUserByID(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateUser: Partial<UserDTO>,
  ): Promise<UserEntity> {
    return await this.userService.updateUserByID(id, updateUser);
  }

  @Delete('/:id')
  async deleteUserByID(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<{ message: string }> {
    return await this.userService.deleteUserByID(id);
  }
}
